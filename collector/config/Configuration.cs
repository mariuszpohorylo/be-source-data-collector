using collector.api;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace collector.config
{
    public class Configuration
    {
        [JsonProperty("byOsIndexUrl")] 
        public static string ByOsIndexUrl { get; set; }
        
        [JsonProperty("beSourceIdIndexUrl")] 
        public static string BeSourceIdIndexUrl { get; set; }

        [JsonProperty("beSourcePersonIndexUrl")]
        public static string BeSourcePersonIndexUrl { get; set; }

        [JsonProperty("entityName")] 
        public static string EntityName { get; set; }
        
        [JsonProperty("endpointType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public static EndpointType EndpointType { get; set; }

        [JsonProperty("timeout")] 
        public static int Timeout { get; set; }
        
        [JsonProperty("jurisdiction")] 
        public static string Jurisdiction { get; set; }
    }
}