using System;
using System.IO;
using Newtonsoft.Json;

namespace collector.config
{
    public static class ConfigReader
    {
        public static void Initialize(string configurationFile = "config.json")
        {
            var filepath = Path.Combine(AppContext.BaseDirectory, configurationFile);
            var stream = new StreamReader(filepath);
            JsonConvert.DeserializeObject<Configuration>(stream.ReadToEnd());
            stream.Dispose();
        }
    }
}