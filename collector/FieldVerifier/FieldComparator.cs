using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using collector.api.besource.sources.dto;
using collector.config;
using collector.csv;
using Newtonsoft.Json;
using NUnit.Framework;

namespace collector.FieldVerifier
{
    public class FieldecComparator
    {
        public IEnumerable<AttributeDto> GetRows()
        {
            var csv = new EntityCsvReader().GetCsvReader("file.csv");
            return csv.GetRecords<AttributeDto>();
        }

        public void Verify()
        {
            var client = Http.GetClient(Configuration.BeSourceIdIndexUrl);
            var csvRows = GetRows();
            
            var response = client.GetAsync("")
                .Result;
            var result =
                response.Content.ReadAsStringAsync()
                    .Result;
            JsonConvert.DeserializeObject<BeSourceDto>(result);
            
            
            foreach (var attributeDto in csvRows)
            {
                var sources = Getsources(attributeDto.Jurisdiction, attributeDto.Name);

                var url = sources.First(item => item.Contains(attributeDto.Source));
                var organization = CallAllSources(url);

                foreach (var organizationResult in organization.Results)
                {
                    Assert.True(organizationResult.Overview.OrganizationName.Equals(attributeDto.Name));
                }
            }
        }
        
        private BeSourceOrganizationDto CallAllSources(string url)
        {
            var client = Http.GetClient(url);

            var response =  client.GetAsync(url)
                .Result;
            if (response.IsSuccessStatusCode)
            {
                string dataObjects = client.GetAsync("")
                    .Result.Content.ReadAsStringAsync()
                    .Result;
                
                var timeout = 0;

                while ((dataObjects.Contains("STARTED") || dataObjects.Contains("RUNNING")) &&
                       timeout < Configuration.Timeout)
                {
                    Thread.Sleep(1000);
                    dataObjects =  client.GetAsync("")
                        .Result.Content.ReadAsStringAsync()
                        .Result;
                    timeout++;
                }
              
                if (!dataObjects.Contains("\"results\": {}") && dataObjects.Contains("results") )
                {
                    Console.WriteLine(dataObjects);
                    return JsonConvert.DeserializeObject<BeSourceOrganizationDto>(dataObjects);
                    Console.WriteLine("Done: " + url);
                }
                else
                {
                    Console.WriteLine("Timeout: " + url);
                    
                }
            }
            client.Dispose();
            return new BeSourceOrganizationDto( );
        }

        public List<string> Getsources(string juridiction, string entityNAme)
        {
            var list = new List<string>();
            BeSourceDto.Results.ForEach(result =>
                list.Add(new UrlBuilder().Url(result.ApiUrl).Jurisdiction(juridiction)
                    .EntityName(entityNAme).Build()));
            return list;
        }



    }
}