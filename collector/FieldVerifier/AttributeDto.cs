using Newtonsoft.Json;

namespace collector.FieldVerifier
{
    public class AttributeDto
    {
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("jurisdiction")] public string Jurisdiction { get; set; }
        [JsonProperty("attribute")] public string Attribute { get; set; }
        [JsonProperty("source")] public string Source { get; set; }
        [JsonProperty("expected")] public string ExpectedResults { get; set; }
    }
}