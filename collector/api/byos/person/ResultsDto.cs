using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.api.byos.person
{
    public class ResultsDto
    {
        [JsonProperty("name")] 
        public string Name;
        
        [JsonProperty("first_name")] 
        public string FirstName;
        
        [JsonProperty("last_name")] 
        public string LastName;
        
        [JsonProperty("date_of_birth")] 
        public string DateOfBirth;
        
        [JsonProperty("place_of_birth")] 
        public string PlaceOfBirth;
        
        [JsonProperty("gender")] 
        public string Gender;
        
        [JsonProperty("nationality")] 
        public string Nationality;

        [JsonProperty("link")] 
        public string Link;
        
        [JsonProperty("source_url")] 
        public string SourceUrl;
        
        [JsonProperty("images")] 
        public List<string> Images;
        
        [JsonProperty("address")] 
        public List<AddressDto> Address;
        
        [JsonProperty("description")] 
        public string Description;

    }
}