using Newtonsoft.Json;

namespace collector.api.byos.person
{
    public class AddressDto
    {
        [JsonProperty("entity_type")] 
        public string EntityType;
        
        [JsonProperty("locality")] 
        public string Locality;
    }
}