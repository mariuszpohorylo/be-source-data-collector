using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.api.byos.person
{
    public class ByosPersonDto : Entity
    {
        [JsonProperty("results")] 
        public List<ResultsDto> Results;
    }
}