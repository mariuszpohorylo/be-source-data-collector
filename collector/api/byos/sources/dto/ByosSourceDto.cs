using Newtonsoft.Json;

namespace collector.api.byos.sources.dto
{
    public class ByosSourceDto : ISource
    {
        [JsonProperty("results")]
        public static ResultsDto Results;
    }
}