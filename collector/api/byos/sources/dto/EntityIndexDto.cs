using Newtonsoft.Json;

namespace collector.api.byos.sources.dto
{
    public class EntityIndexDto
    {
        [JsonProperty("apiurl")] 
        public string ApiUrl;
        
        [JsonProperty("api_base_url")] 
        public string ApiBaseUrl;
    }
}