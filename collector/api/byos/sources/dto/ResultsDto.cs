using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.api.byos.sources.dto
{
    public class ResultsDto
    {
        [JsonProperty("person")] 
        public List<EntityIndexDto> Person;
        
        [JsonProperty("organization")] 
        public List<EntityIndexDto> Organization;
    }
}