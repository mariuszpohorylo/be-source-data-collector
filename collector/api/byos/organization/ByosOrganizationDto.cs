using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.api.byos.organization
{
    public class ByosOrganizationDto : Entity
    {
        [JsonProperty("results")] 
        public  List<ResultsDto> Results;
    }
}