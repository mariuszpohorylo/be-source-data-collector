using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.api.byos.organization
{
    public class ResultsDto
    {
        [JsonProperty("name")] 
        public string Name;
        
        [JsonProperty("source_url")] 
        public string SourceUrl;
        
        [JsonProperty("address")] 
        public List<RegisteredAddressDto> Address;
        
        [JsonProperty("juridiction")] 
        public string Jurisdiction;
        
        [JsonProperty("aka")] 
        public List<string> Aka;
        
        [JsonProperty("place_of_registration")] 
        public string PlaceOfRegistration; //demciled in
        
        [JsonProperty("identifiers")] 
        public IdentifiersDto Identifiers;
        
    }
}