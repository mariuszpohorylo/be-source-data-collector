using Newtonsoft.Json;

namespace collector.api.byos.organization
{
    public class RegisteredAddressDto
    {
        [JsonProperty("country")] 
        public string Country;

        [JsonProperty("city")] 
        public string City;

        [JsonProperty("street_address")] 
        public string StreetAddress;

        [JsonProperty("zip")] 
        public string Zip;

        [JsonProperty("full_address")] 
        public string FullAddress;

        public override string ToString()
        {
            return
                $"[Country]: {Country?.ToString()}, [City]: {City?.ToString()}, [StreetAddress]: "
                + $"{StreetAddress?.ToString()}, [Zip]: {Zip?.ToString()}, [FullAddress]: {FullAddress?.ToString()}";
        }
    }
}