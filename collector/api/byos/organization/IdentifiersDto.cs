using Newtonsoft.Json;

namespace collector.api.byos.organization
{
    public class IdentifiersDto
    {
        [JsonProperty("trade_register_number")]
        public string TradeRegisterNumber;

        [JsonProperty("vat_tax_number")] 
        public string VatTaxNumber;

        [JsonProperty("european_vat_number")] 
        public string EuropeanVatNumber;

        [JsonProperty("legal_entity_identifier")]
        public string LegalEntityIdentifier;

        [JsonProperty("other_company_id_number")]
        public string OtherCompanyIdNumber;

        [JsonProperty("international_securities_identifier")]
        public string InternationalSecuritiesIdentifier;

        public override string ToString()
        {
            return
                $"[TradeRegisterNumber]: {TradeRegisterNumber?.ToString()}, [VatTaxNumber]: {VatTaxNumber?.ToString()}, "
                + $"[EuropeanVatNumber]: {EuropeanVatNumber?.ToString()}, [LegalEntityIdentifier]: {LegalEntityIdentifier?.ToString()}, "
                + $"[OtherCompanyIdNumber]: {OtherCompanyIdNumber?.ToString()}, "
                + $"[InternationalSecuritiesIdentifier]: {InternationalSecuritiesIdentifier?.ToString()}";
        }
    }
}