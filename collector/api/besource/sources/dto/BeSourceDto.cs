using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.api.besource.sources.dto
{
    public class BeSourceDto : ISource
    {
        [JsonProperty("results")]
        public static List<ResultsDto> Results;
    }
}