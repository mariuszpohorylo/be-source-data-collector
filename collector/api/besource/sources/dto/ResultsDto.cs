using Newtonsoft.Json;

namespace collector.api.besource.sources.dto
{
    public class ResultsDto
    {
        [JsonProperty("apiurl")]
        public string ApiUrl;
        [JsonProperty("api_base_url")]
        public string ApiBaseUrl;
    }
}