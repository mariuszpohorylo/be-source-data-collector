using System.Collections.Generic;
using collector.organization;
using Newtonsoft.Json;


namespace collector
{
    public class BeSourceOrganizationDto : Entity
    {
        [JsonProperty("results")] public List<Results> Results { get; set; }
    }
}