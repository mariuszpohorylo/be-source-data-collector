using System.Collections.Generic;
using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;

namespace collector.organization
{
    public class Overview
    {
        [JsonProperty("@source-id")]
        public string SourceId { get; set; }
        
        [JsonProperty("bst:registryURI")]
        public string RegistryURI { get; set; }

        [JsonProperty("vcard:organization-name")]
        public string OrganizationName { get; set; }

        [JsonProperty("bst:aka")] public List<string> AliasName { get; set; } //alias name

        // [JsonProperty("isIncorporatedIn")] public List<string> IncorporatedIn{ get; set; }
        [JsonProperty("isIncorporatedIn")] public string IncorporatedIn { get; set; }

        [JsonProperty("mdaas:RegisteredAddress")]
        public RegisteredAddress RegisteredAddress { get; set; }

        [JsonProperty("tr-org:hasRegisteredPhoneNumber")]
        public string RegisteredPhoneNumber { get; set; }

        [JsonProperty("bst:stock_info")] public StockInfo StockInfo { get; set; }
        [JsonProperty("isDomiciledIn")] public string IsDomiciledIn { get; set; } //jurisdiction
        [JsonProperty("hasURL")] public string HasURL { get; set; }
        [JsonProperty("hasIPODate")] public string HasIPODate { get; set; }
        [JsonProperty("identifiers")] public Identifiers Identifiers { get; set; }
    }
}