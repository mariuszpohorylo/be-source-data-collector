using Newtonsoft.Json;

namespace collector.organization
{
    public class Identifiers
    {
        [JsonProperty("trade_register_number")]
        public string TradeRegisterNumber { get; set; }

        [JsonProperty("vat_tax_number")] public string VatTaxNumber { get; set; }
        [JsonProperty("european_vat_number")] public string EuropeanVatNumber { get; set; }

        [JsonProperty("legal_entity_identifier")]
        public string LegalEntityIdentifier { get; set; }

        [JsonProperty("other_company_id_number")]
        public string OtherCompanyIdNumber { get; set; }

        [JsonProperty("swift_code")] public string SwiftCode { get; set; }
        [JsonProperty("giin")] public string Giin { get; set; }

        public override string ToString()
        {
            return
                $"[Trade_register_number]: {TradeRegisterNumber?.ToString()}, [Vat_tax_number]: {VatTaxNumber?.ToString()}, "
                + $"[european_tax_number]:{EuropeanVatNumber?.ToString()},"
                + $"[legal_entity_identifier]: {LegalEntityIdentifier?.ToString()}, [other_company_id_number]: "
                + $"{OtherCompanyIdNumber?.ToString()}, [swift_code]: {SwiftCode?.ToString()}, [giin]: {Giin?.ToString()}";
        }
    }
}