using System.Globalization;
using Newtonsoft.Json;

namespace collector.organization
{
    public class RegisteredAddress
    {
        [JsonProperty("country")] public string Country { get; set; }
        [JsonProperty("fullAddress")]  public string FullAddress { get; set; }
        [JsonProperty("streetAddress")]  public string StreetAddress { get; set; }
        [JsonProperty("city")]  public string City { get; set; }
        [JsonProperty("zipcode")]  public string Zipcode { get; set; }

        public override string ToString()
        {
            return $"[country]: {Country?.ToString()}, [fullAddress]: {FullAddress?.ToString()} "
                   + $"[streetAddress]: {StreetAddress?.ToString()}, [city]: {City?.ToString()} "
                   + $"[zipcode]: {Zipcode?.ToString()}";
        }
    }
}