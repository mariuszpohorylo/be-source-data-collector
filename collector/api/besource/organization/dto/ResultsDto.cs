using System.Collections.Generic;
using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;

namespace collector.organization
{
    public class Results
    {
        [JsonProperty("bst:registryURI")] public string RegistryURI { get; set; }
        [JsonProperty("overview")] public Overview Overview { get; set; }
    }
}