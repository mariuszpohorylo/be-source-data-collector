using Newtonsoft.Json;

namespace collector.organization
{
    public class StockInfo
    {
        [JsonProperty("ticket_symbol")] public string TicketSymbol { get; set; }
        [JsonProperty("main_exchange")] public string MainExchange { get; set; }

        public override string ToString()
        {
            return $"[Ticket symbol]: {TicketSymbol?.ToString()}, [Main exchange]: {MainExchange?.ToString()}";
        }
    }
}