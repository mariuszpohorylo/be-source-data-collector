using System.Collections.Generic;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace collector.sections
{
    public class OverviewDto
    {
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("givenName")] public string GivenName { get; set; }
        [JsonProperty("familyName")] public string FamilyName { get; set; }
        [JsonProperty("height")] public string Height { get; set; }
        [JsonProperty("sourceURL")] public string SourceURL { get; set; }
        [JsonProperty("jobTitle")] public string JobTitle { get; set; }
        [JsonProperty("gender")] public string Gender { get; set; }
        [JsonProperty("nationality")] public string Nationality { get; set; }
        [JsonProperty("birthDate")] public string BirthDate { get; set; }
        [JsonProperty("honorificPrefix")] public string HonorificPrefix { get; set; }
        [JsonProperty("honorificSuffix")] public string HonorificSuffix { get; set; }
        [JsonProperty("additionalName")] public string[] AdditionalName { get; set; }
        [JsonProperty("email")] public string[] Email { get; set; }
        [JsonProperty("image")] public string Image { get; set; }
        [JsonProperty("address")] public List<AddressDto> Address { get; set; }
        [JsonProperty("homeLocation")] public HomeLocation HomeLocation { get; set; }
        //birthplace
    }
}