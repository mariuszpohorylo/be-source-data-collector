using System.Collections.Generic;
using Newtonsoft.Json;

namespace collector.sections
{
    public class Results
    {
        [JsonProperty("search_query")] public string SearchedQuery { get; set; }
        [JsonProperty("overview")] public OverviewDto OverviewDto { get; set; }
        [JsonProperty("_links")] public Links Links { get; set; }
    }
}