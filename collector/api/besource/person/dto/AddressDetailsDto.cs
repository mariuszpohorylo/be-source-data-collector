using Newtonsoft.Json;

namespace collector.sections
{
    public class AddressDetails
    {
        [JsonProperty("addressCountry")] public string AddressCountry { get; set; }
        [JsonProperty("addressLocality")] public string AddressLocality { get; set; }
        [JsonProperty("addressRegion")] public string AddressRegion { get; set; }
        [JsonProperty("postalCode")] public string PostalCode { get; set; }
    }
}