using System.Collections.Generic;
using System.Threading;
using collector.sections;
using Newtonsoft.Json;

namespace collector
{
    public class BeSourcePersonDto : Entity
    {
        [JsonProperty("results")] public List<Results> Results { get; set; }
    }
}