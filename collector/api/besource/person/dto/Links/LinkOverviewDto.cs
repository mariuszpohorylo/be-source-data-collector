using Newtonsoft.Json;

namespace collector.sections
{
    public class LinkOverviewDto
    {
        [JsonProperty("method")] public string Method { get; set; }
        [JsonProperty("Url")] public string Url { get; set; }
    }
}