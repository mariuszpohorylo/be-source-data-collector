using Newtonsoft.Json;

namespace collector.sections
{
    public class Links
    {
        [JsonProperty("overview")] public LinkOverviewDto LinkOverviewDto { get; set; }
    }
}