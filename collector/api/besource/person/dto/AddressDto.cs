using System;
using Newtonsoft.Json;

namespace collector.sections
{
    public class AddressDto
    {
        [JsonProperty("streetAddress")] public string StreetAddress {get; set;}
        [JsonProperty("city")] public string City {get; set;}
        [JsonProperty("addressLocality")] public string AddressLocality {get; set;}
        [JsonProperty("addressRegion")] public string AddressRegion {get; set;}
        [JsonProperty("country")] public string Country {get; set;}
    }
}