using Newtonsoft.Json;

namespace collector.sections
{
    public class BirthPlaceDto
    {
        [JsonProperty("address")] public AddressDetails Address { get; set; }
    }
}