using Newtonsoft.Json;

namespace collector.sections
{
    public class HomeLocation
    {
        [JsonProperty("address")] public AddressDetails Address;
    }
}