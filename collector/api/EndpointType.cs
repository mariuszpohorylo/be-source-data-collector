namespace collector.api
{
    public enum EndpointType
    {
        BeSourcePerson,
        BeSourceOrganization,
        ByosPerson,
        ByosOrganization
    }
}