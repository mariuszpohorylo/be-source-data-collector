using System;

namespace collector
{
    public class UrlBuilder
    {
        private string uri;

        public UrlBuilder Url(string baseURl)
        {
            uri = baseURl;
            return this;
        }

        public UrlBuilder EntityName(string entityName)
        {
            uri = uri.Replace("{name}", entityName);
            uri = uri.Replace("{query}", entityName);
            return this;
        }

        public UrlBuilder Jurisdiction(string jurisdiction)
        {
            if (!string.IsNullOrEmpty(jurisdiction))
            {
                uri += "&jurisdiction=" + jurisdiction;
            }
            
            return this;
        }
        
        public UrlBuilder Source(string source)
        {
            if (!string.IsNullOrEmpty(source))
            {
                uri += "?source=" + source;
            }
            
            return this;
        }

        public string Build()
        {
            Console.WriteLine("build: " + uri);
            return uri;
        }
    }
}