using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace collector
{
    public static class Http
    {
        private const string MEDIA_TYPE_HEADER = "application/json";

        public static HttpClient GetClient(string url)
        {
            var client = new HttpClient {BaseAddress = new Uri(url)};
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MEDIA_TYPE_HEADER));
            return client;
        }
    }
}