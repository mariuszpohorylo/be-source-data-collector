using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using collector.api;
using collector.api.besource.sources.dto;
using collector.api.byos.sources.dto;
using collector.config;
using Newtonsoft.Json;

namespace collector
{
    public class SourceIndexReader
    {
        public List<string> GetSourceEndpoints<T>(string url) where T : ISource
        {
            var client = Http.GetClient(url);
            var response = client.GetAsync("")
                .Result;

            if (response.IsSuccessStatusCode)
            {
                var result =
                    response.Content.ReadAsStringAsync()
                        .Result;
                JsonConvert.DeserializeObject<T>(result);
            }

            client.Dispose();

            return ConvertIndexResultToUriList();
        }

        private static List<string> ConvertIndexResultToUriList()
        {
            var urls = new List<string>();

            switch (Configuration.EndpointType)
            {
                case EndpointType.ByosPerson:
                    urls = GetByosPersonSources();
                    break;
                case EndpointType.ByosOrganization:
                    urls = GetByosOrganizationSources();
                    break;
                case EndpointType.BeSourcePerson:
                case EndpointType.BeSourceOrganization:
                    urls = GetBeSourceSources();
                    break;
                default:
                    throw new ArgumentException(
                        "Incorrect EndpointType argument. Correct argument: BeSourcePerson, BeSourceOrganization, ByosPerson, ByosOrganization");
            }

            return urls;
        }

        private static List<string> GetByosPersonSources()
        {
            var list = new List<string>();
            ByosSourceDto.Results.Person.ForEach(x =>
                list.Add(new UrlBuilder().Url(x.ApiUrl).Jurisdiction(Configuration.Jurisdiction)
                    .EntityName(Configuration.EntityName).Build()));
            return list;
        }

        private static List<string> GetByosOrganizationSources()
        {
            var list = new List<string>();
            ByosSourceDto.Results.Organization.ForEach(source => list.Add(new UrlBuilder().Url(source.ApiUrl)
                .Jurisdiction(Configuration.Jurisdiction)
                .EntityName(Configuration.EntityName).Build()));
            return list;
        }

        private static List<string> GetBeSourceSources()
        {
            var list = new List<string>();
            BeSourceDto.Results.ForEach(result =>
                list.Add(new UrlBuilder().Url(result.ApiUrl).Jurisdiction(Configuration.Jurisdiction)
                    .EntityName(Configuration.EntityName).Build()));
            return list;
        }
    }
}