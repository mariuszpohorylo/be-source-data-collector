using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using collector.config;
using Newtonsoft.Json;

namespace collector
{
    public class SourceReader
    {
        public readonly List<Entity> EntitiesList = new List<Entity>();

        private async void CallAllSources<K>(string url) where K : Entity
        {
            var client = Http.GetClient(url);

            var response =  client.GetAsync(url)
                .Result;
            if (response.IsSuccessStatusCode)
            {
                string dataObjects = await GetContentAsync(client); 

                var timeout = 0;

                while ((dataObjects.Contains("STARTED") || dataObjects.Contains("RUNNING")) &&
                       timeout < Configuration.Timeout)
                {
                    Thread.Sleep(1000);
                    dataObjects =  await GetContentAsync(client);
                    timeout++;
                }
              
                if (!dataObjects.Contains("\"results\": {}") && dataObjects.Contains("results") )
                {
                    Console.WriteLine(dataObjects);
                    EntitiesList.Add(JsonConvert.DeserializeObject<K>(dataObjects));
                    Console.WriteLine("Done: " + url);
                }
                else
                {
                    Console.WriteLine("Timeout: " + url);
                }
            }
            client.Dispose();
        }

        private async Task<string> GetContentAsync(HttpClient client)
        {
            return await Task.Run(() => client.GetAsync("")
                .Result.Content.ReadAsStringAsync()
                .Result);
        }
        
        public void CallEndpoint<T, K>(string sourceUrl) where T : ISource  where K : Entity
        {
            Console.WriteLine("start           ");
            var indexes = new SourceIndexReader().GetSourceEndpoints<T>(sourceUrl);
            foreach (var url in indexes)
            {
                CallAllSources<K>(url);
            }
        }

    }
}