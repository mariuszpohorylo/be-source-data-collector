﻿using System;
using collector.api;
using collector.api.byos.person;
using collector.config;
using collector.api.besource.sources.dto;
using collector.api.byos.organization;
using collector.api.byos.sources.dto;
using collector.csv.byos;
using collector.csv;

namespace collector
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigReader.Initialize();

            var a = new SourceReader();
            EntityCsvWriter pw;
            switch (Configuration.EndpointType)
            {
                case EndpointType.BeSourcePerson:
                    pw = new BeSourcePersonCsvWriter();
                    a.CallEndpoint<BeSourceDto, BeSourcePersonDto>(Configuration.BeSourcePersonIndexUrl);
                    pw.WriteToCsv(a.EntitiesList);
                    break;
                case EndpointType.BeSourceOrganization:
                    pw = new BeSourceOrganizationCsvWriter();
                    a.CallEndpoint<BeSourceDto, BeSourceOrganizationDto>(Configuration.BeSourceIdIndexUrl);
                    pw.WriteToCsv(a.EntitiesList);
                    break;
                case EndpointType.ByosPerson:
                    pw = new ByosPersonCsvWriter();
                    a.CallEndpoint<ByosSourceDto, ByosPersonDto>(Configuration.ByOsIndexUrl);
                    pw.WriteToCsv(a.EntitiesList);
                    break;
                case EndpointType.ByosOrganization:
                    pw = new ByosOrganizationCsvWriter();
                    a.CallEndpoint<ByosSourceDto, ByosOrganizationDto>(Configuration.ByOsIndexUrl);
                    pw.WriteToCsv(a.EntitiesList);
                    break;
                default:
                    throw new ArgumentException(
                        "Incorrect EndpointType argument. Correct argument: BeSourcePerson, BeSourceOrganization, ByosPerson, ByosOrganization");
            }
        }
    }
}