using System.Collections.Generic;
using collector.api.byos.person;

namespace collector.csv.byos
{
    public class ByosPersonCsvWriter : EntityCsvWriter
    {
        protected override IEnumerable<string> Headers => new[]
        {
            "SourceUrl", "Name", "FirstName", "LastName", "DateOfBirth", "Gender", "Nationality",
            "PlaceOfBirth", "Address", "Description", "Images", "Link"
        };

        public override void WriteToCsv(IEnumerable<Entity> persons)
        {
            var csv = GetCsvWriter();

            foreach (ByosPersonDto person in persons)
            {
                foreach (var i in person.Results)
                {
                    csv.NextRecord();
                    csv.WriteField(i.SourceUrl);
                    csv.WriteField(i.Name);
                    csv.WriteField(i.FirstName);
                    csv.WriteField(i.LastName);
                    csv.WriteField(i.DateOfBirth);
                    csv.WriteField(i.Gender);
                    csv.WriteField(i.Nationality);
                    csv.WriteField(i.PlaceOfBirth);
                    csv.WriteField(i.Address?.ToArray().ToString());
                    csv.WriteField(i.Description);
                    csv.WriteField(i.Images);
                    csv.WriteField(i.Link);
                }
            }

            csv.Flush();
            csv.Dispose();
        }
    }
}