using System.Collections.Generic;
using collector.api.byos.organization;
using CsvHelper;
using collector.csv;

namespace collector.csv.byos
{
    public class ByosOrganizationCsvWriter : EntityCsvWriter
    {
        protected override IEnumerable<string> Headers => new[]
        {
            "SourceUrl", "Name", "Jurisdiction", "PlaceOfRegistration", "Identifiers", "Address"
        };

        public override void WriteToCsv(IEnumerable<Entity> organizations)
        {
            var csv = GetCsvWriter();

            foreach (ByosOrganizationDto org in organizations)
            {
                foreach (var i in org.Results)
                {
                    csv.NextRecord();
                    csv.WriteField(i.SourceUrl);
                    csv.WriteField(i.Name);
                    csv.WriteField(i.Jurisdiction);
                    csv.WriteField(i.PlaceOfRegistration);
                    csv.WriteField(i.Identifiers?.ToString());
                    csv.WriteField(i.Aka?.ToString()); ///array
                    csv.WriteField(i.Address?.ToArray().ToString());
                }
            }

            csv.Flush();
            csv.Dispose();
        }
    }
}