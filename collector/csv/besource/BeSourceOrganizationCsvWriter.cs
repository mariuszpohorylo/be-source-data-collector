using System.Collections.Generic;
using CsvHelper;
using collector.csv;

namespace collector
{
    public class BeSourceOrganizationCsvWriter : EntityCsvWriter
    {
        protected override IEnumerable<string> Headers => new[]
        {
            "registryURI", "overview.registryURI", "overview.sourceId", "overview.OrganizationName",
            "overview.AliasName", "overview.IncorporatedIn",
            "overview.StockInfo", "overview.registeredAddress", "overview.registeredPhoneNumber",
            "overview.isDomiciledIn", "overview.hasURL", "overview.hasIPODate", "overview.IdentifiersDto"
        };

        public override void WriteToCsv(IEnumerable<Entity> organizations)
        {
            var csv = GetCsvWriter();
            
            foreach (BeSourceOrganizationDto org in organizations)
            {
                foreach (var i in org.Results)
                {
                    csv.NextRecord();
                    csv.WriteField(i.RegistryURI);
                    csv.WriteField(i.Overview.RegistryURI);
                    csv.WriteField(i.Overview.SourceId);
                    csv.WriteField(i.Overview.OrganizationName);
                    csv.WriteField(i.Overview.AliasName?.ToArray().ToString());
                    csv.WriteField(i.Overview.IncorporatedIn);
                    csv.WriteField(field: i.Overview.StockInfo?.ToString());
                    csv.WriteField(i.Overview.RegisteredAddress?.ToString());
                    csv.WriteField(i.Overview.RegisteredPhoneNumber);
                    csv.WriteField(i.Overview.IsDomiciledIn);
                    csv.WriteField(i.Overview.HasURL);
                    csv.WriteField(i.Overview.HasIPODate);
                    csv.WriteField(i.Overview.Identifiers?.ToString());
                }
            }
            csv.Flush();
            csv.Dispose();
        }
    }
}