using System.Collections.Generic;
using CsvHelper;
using collector.csv;

namespace collector
{
    public class BeSourcePersonCsvWriter : EntityCsvWriter
    {
        protected override IEnumerable<string> Headers => new[]
        {
            "Overview.SourceURL", "Overview.Name", "Overview.GivenName", "Overview.FamilyName",
            "Overview.AdditionalName", "Overview.Nationality", "Overview.JobTitle", "Overview.BirthDate", 
            "Overview.Gender", "Overview.Email", "Overview.Image", "Overview.HonorificPrefix", 
            "Overview.HonorificSuffix", "Overview.Address", "Overview.HomeLocation", "SearchedQuery"
        };

        public override void WriteToCsv(IEnumerable<Entity> persons)
        {
            var csv = GetCsvWriter();

            foreach (BeSourcePersonDto person in persons)
            {
                foreach (var i in person.Results)
                {
                    csv.NextRecord();
                    csv.WriteField(i.OverviewDto.SourceURL);
                    csv.WriteField(i.OverviewDto.Name);
                    csv.WriteField(i.OverviewDto.GivenName);
                    csv.WriteField(i.OverviewDto.FamilyName);
                    csv.WriteField(i.OverviewDto.AdditionalName);
                    csv.WriteField(i.OverviewDto.Nationality);
                    csv.WriteField(i.OverviewDto.JobTitle);
                    csv.WriteField(i.OverviewDto.BirthDate);
                    csv.WriteField(i.OverviewDto.Gender);
                    csv.WriteField(i.OverviewDto.Email);
                    csv.WriteField(i.OverviewDto.Image);
                    csv.WriteField(i.OverviewDto.HonorificPrefix);
                    csv.WriteField(i.OverviewDto.HonorificSuffix);
                    csv.WriteField(i.OverviewDto.Address?.ToArray().ToString());
                    csv.WriteField(i.OverviewDto.HomeLocation?.Address);
                    csv.WriteField(i.SearchedQuery);
                }
            }

            csv.Flush();
            csv.Dispose();
        }
    }
}