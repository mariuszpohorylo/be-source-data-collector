using System.Globalization;
using System.IO;
using CsvHelper;

namespace collector.csv
{
    public class EntityCsvReader
    {
        public CsvReader GetCsvReader(string pathToFile)
        {
            var reader = new StreamReader(pathToFile);
            var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv;
        }
    }
}