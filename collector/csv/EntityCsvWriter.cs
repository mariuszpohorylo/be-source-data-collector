using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using collector.config;
using CsvHelper;

namespace collector.csv
{
    public abstract class EntityCsvWriter
    {
        private const string Delimiter = ";";
        protected abstract IEnumerable<string> Headers { get; }

        public abstract void WriteToCsv(IEnumerable<Entity> entity);

        protected CsvWriter GetCsvWriter()
        {
            var writer = new StreamWriter(GetFileName());
            var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csv.Configuration.Delimiter = Delimiter;
            csv.WriteRecords(Headers); //records or fields?
            return csv;
        }

        private string GetFileName()
        {
            return $"{DateTime.Now:MM-dd-hhmm}_{Configuration.EndpointType}_{Configuration.EntityName}.csv";
        }
    }
}